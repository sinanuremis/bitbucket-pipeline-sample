var https = require("https");

module.exports = {
  "@tags": ["bitbucket_nightwatch_sample", "todo"],

  "Todo List": function (client) {
    client
      .url("http://localhost:8888")
      .pause(2000)
      .waitForElementVisible('input[id=addbutton]', 5000)
      .click('input[name=li1]')
      .pause(2000)
      .click('input[name=li3]')
      .pause(1000)
      .click('input[name=li4]')
      .pause(1000)
      .click('input[id=sampletodotext]')
      .setValue("input[id=sampletodotext]", "Let's add new to do item")
      .click('input[id=addbutton]')
      .assert.title("Sample page - lambdatest.com")
      .pause(1000)
      .end();
  },

  afterEach: function (client, done) {
    setTimeout(function () {
      done();
    }, 1000);
  },
};
